#!/bin/sh

# This shell script is developed to automate the installation process of EtherLAB Master Module API by making all the necessary
# settings to the required files.

#   CSL-EP Legged Robots Team - National Technical University of Athens

#   Developers:
#               Thanasis Mastrogeorgiou, PhD Student
#               Aristotelis Papatheodorou, Undergraduate Student

#   Based on the diploma thesis of Mike Karamousadakis @ CSL, supervised by A. S. Mastrogeorgiou, K. Machairas and E. G. Papadopoulos

# The user should run the script as follows:
# ./install_etherlab.sh [Network Adapter Interface Name] [EtherLAB Mercurial Changeset]

# To find your device's network adapter interfaces run in a terminal: ifconfig | grep -e "^e[tn][a-z0-9]*" -o
# To review and choose the desired EtherLAB Mercurial Changeset to install, navigate to: http://hg.code.sf.net/p/etherlabmaster/code
#
# Shell script variables.
InterfaceName=$1
interfaceMAC=`cat /sys/class/net/$InterfaceName/address`;
ethercatUserGroup=`whoami`;
ethercatMasterDirectory="ethercat-1.5.2-merc";
udevRulesFile="99-EtherCAT.rules";
#
# Rules File Creation.
echo "Generating udev rules file";
echo 'KERNEL=="EtherCAT[0-9]*"', 'MODE="0664"', 'GROUP=''"'$ethercatUserGroup'"'>$udevRulesFile;
#
# Install Necessary Mercurial tools.
sudo apt-get install autoconf automake libtool mercurial librospack-dev
#
# Clone EtherLAB specific (2nd terminal argument) version from http://hg.code.sf.net/p/etherlabmaster/code repository.
hg clone -u $2 http://hg.code.sf.net/p/etherlabmaster/code ethercat-1.5.2-merc #e.g.: 334c34cfd2e5 (recommended) or 33b922ec1871 
#
# Install the necessary packages for building EtherLab.
#
# hg clone might fail because there is no user registered. In this case uncomment and run the following line:
# echo -e '[extensions] \n mq = \n [ui] \n username = Foo Bar <foobar@mail.com>' > ~/.hgrc
#
# Build EtherLAB Master.
#
cd $ethercatMasterDirectory;
./bootstrap;
# Choose EtherLAB network Adapter Driver: --Generic is the Default Option.
#                                                                                       --disable-cycles if your CPU is not Intel.
./configure --prefix /opt/etherlab --enable-generic --disable-8139too  --enable-hrtimer --enable-cycles;\
make all modules;
sudo make modules_install install;
sudo depmod;
sudo mv ../$udevRulesFile /etc/udev/rules.d/$udevRulesFile;
sudo ln -s /opt/etherlab/etc/init.d/ethercat /etc/init.d/ethercat;
sudo mkdir -p /etc/sysconfig/;
sudo cp /opt/etherlab/etc/sysconfig/ethercat /etc/sysconfig/ethercat;
sudo sed -i 's/DEVICE_MODULES=\"\"/DEVICE_MODULES=\"generic\"/g' /etc/sysconfig/ethercat;
sudo ln -s /opt/etherlab/bin/ethercat /usr/bin/ethercat;
sudo sed -i "s/MASTER0_DEVICE=\"\"/MASTER0_DEVICE=\"$interfaceMAC\"/g" /etc/sysconfig/ethercat;
#
# Set EtherLAB service & Initialize it.
sudo update-rc.d ethercat defaults;
sudo /etc/init.d/ethercat start;
#
# Clean the Installation Files.
# echo "Removing compiled installation files";
# rm -f -r $ethercatMasterDirectory $udevRulesFile;
#
# For Debug Purposes
# echo "interface  $interfaceMAC";
# echo "user  $ethercatUserGroup";
# echo "$ethercatMasterDirectory";
# cd $ethercatMasterDirectory
# temp=`pwd`
# echo "$temp";
#
#       ~END OF FILE~