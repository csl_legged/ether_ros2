/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 Control Systems Lab, Legged Robots Team, NTUA
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free software; you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is distributed in the hope that
 *  it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment. If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *  Contact information: csl.legged.ntua@gmail.com
 *****************************************************************************/
/**
   \file ethercat_communicator.h
   \brief Header file for the EthercatCommunicator class
*/

/*****************************************************************************/
#ifndef ETH_COM_LIB_H
#define ETH_COM_LIB_H

#include <iostream>
#include <pthread.h>
#include <variant>
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "lifecycle_msgs/msg/transition.hpp"
#include "ether_ros2/visibility_control.h"

#include "ether_ros2/msg/pdo_in.hpp"
#include "ether_ros2/msg/pdo_out.hpp"
#include "ether_ros2/msg/pdo_in_xmc.hpp"
#include "ether_ros2/msg/pdo_raw.hpp"
#include "ether_ros2/msg/modify_pdo_variables.hpp"

#include <sched.h>

namespace ether_ros2
{
#define DC_FILTER_CNT 1024
#if !defined(SYNC_MASTER_TO_REF) && !defined(SYNC_REF_TO_MASTER)

#define SYNC_REF_TO_MASTER //the default synchronization will be ref to master

#endif
#if !defined(FIFO_SCHEDULING) && !defined(DEADLINE_SCHEDULING)

#define FIFO_SCHEDULING //the default scheduling policy will be FIFO

#endif

class EthercatCommunicator : public rclcpp_lifecycle::LifecycleNode
{
private:

  //
  // EtherCAT Communicator Private Members
  //
  pthread_attr_t current_thattr_;
  struct sched_param sched_param_;
  static int cleanup_pop_arg_;
  //cleanup_pop_arg_ is used only for future references. No actual usage in our application.
  //Serves as an argument to the cleanup_handler.
  static pthread_t communicator_thread_;
  static bool running_thread_;
  static uint64_t dc_start_time_ns_;
  static uint64_t dc_time_ns_;
  static int64_t system_time_base_;

  #ifdef SYNC_MASTER_TO_REF
  static uint8_t dc_started_;
  static int32_t dc_diff_ns_;
  static int32_t prev_dc_diff_ns_;
  static int64_t dc_diff_total_ns_;
  static int64_t dc_delta_total_ns_;
  static int dc_filter_idx_;
  static int64_t dc_adjust_ns_;
  #endif
  static void *run(void *arg);
  static void cleanup_handler(void *arg);
  static void copy_data_to_domain_buf();
  static void sync_distributed_clocks(void);
  static void update_master_clock(void);
  static uint64_t system_time_ns(void);
  
  // #ifdef LOGGING
  // static void create_new_statistics_sample(statistics_struct *ss, unsigned int * sampling_counter);
  // static void create_statistics(statistics_struct * ss, struct timespec * wakeup_time_p);
  // static void log_statistics_to_file(statistics_struct *ss);
  // #endif

  rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoRaw>::SharedPtr pdo_raw_pub_;

  //
  // EtherCAT Communicator RAW Publisher Initialization
  //
  void raw_publisher_init(void);

  //
  // Clear Node Elements Routine
  //
  void ClearNodeElements(void);

  //
  // PDO In Publisher Private Members
  //
  rclcpp::Subscription<ether_ros2::msg::PdoRaw>::SharedPtr pdo_in_raw_sub_;
  // std::vector<std::variant<std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoIn>>, 
  //                   std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoInXmc>>>> pdo_in_pub_;
  std::vector<std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoIn>>> pdo_in_pub_;
  void pdo_in_init(void);

  //
  // PDO Out Listener Private Members
  //
  rclcpp::Subscription<ether_ros2::msg::ModifyPdoVariables>::SharedPtr pdo_out_listener_;

  std::map<std::string, int> int_type_map_ = {
  {"bool", 0},
  {"uint8", 1},
  {"int8", 2},
  {"uint16", 3},
  {"int16", 4},
  {"uint32", 5},
  {"int32", 6},
  {"uint64", 7},
  {"int64", 8}
  };

  void pdo_out_listener_init(void);
  void modify_pdo_variable(int pos, ether_ros2::msg::ModifyPdoVariables::SharedPtr new_var);

  //
  // PDO Out Publisher Private Members
  //
  rclcpp::Subscription<ether_ros2::msg::PdoRaw>::SharedPtr pdo_out_raw_sub_;
  rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoOut>::SharedPtr pdo_out_pub_;

  void pdo_out_publisher_init(void);

  //
  // PDO Out Publisher Timer Private Members
  //
   rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoOut>::SharedPtr pdo_out_timer_pub_;
   rclcpp::TimerBase::SharedPtr pdo_out_timer_;
   std::shared_ptr<std::uint8_t[]> *data_ptr_;
   void pdo_out_publisher_timer_init(void);


public:

  //
  // EtherCAT Communicator Public Members
  //
  //COMPOSITION_PUBLIC
  explicit EthercatCommunicator(rclcpp::NodeOptions options);
  ~EthercatCommunicator();

  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
  on_configure(const rclcpp_lifecycle::State & previous_state);

  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
  on_activate(const rclcpp_lifecycle::State & previous_state);

  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
  on_deactivate(const rclcpp_lifecycle::State & previous_state);

  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
  on_cleanup(const rclcpp_lifecycle::State & previous_state);

  rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
  on_shutdown(const rclcpp_lifecycle::State & previous_state);

  static bool has_running_thread(void);

  bool start(void);
  bool stop(void);

  //
  // EtherCAT Communicator Initializaton Function
  //
  void ecat_comm_init(void);

  //
  // PDO Raw Publisher Routine
  //
  void publish_raw_data(void);

  //
  // PDO In Publisher Public Members
  //
  void pdo_in_raw_callback(ether_ros2::msg::PdoRaw::UniquePtr pdo_raw);

  //
  // PDO Out Listener Private Members
  //
  void pdo_out_callback(ether_ros2::msg::ModifyPdoVariables::SharedPtr new_var);

  //
  // PDO Out Publisher Public Members
  //
  void pdo_out_raw_callback(ether_ros2::msg::PdoRaw::SharedPtr pdo_raw);

  //
  // PDO Out Publisher Timer Public Members
  //
  void timer_callback(std::weak_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PdoOut>> captured_pub);

};

}
#endif /* ETH_COM_LIB_H */
