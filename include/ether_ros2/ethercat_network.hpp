/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 Control Systems Lab, Legged Robots Team, NTUA
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *  Contact information: csl.legged.ntua@gmail.com
 *****************************************************************************/
/**
   \file ether_ros.h
   \brief Main header file.
*/

#ifndef ether_ros_LIB_H
#define ether_ros_LIB_H

#include "rclcpp/rclcpp.hpp"
// #include <errno.h>
#include <stdio.h>
// #include <string.h>
// #include <sys/resource.h>
#include <sys/types.h>
// #include <sys/stat.h>
// #include <fcntl.h>
#include <unistd.h>
// #include <time.h>
#include <stddef.h>
#include <sys/mman.h>

#include <memory>

#ifdef __cplusplus
extern "C"
{
#endif
# include "ecrt.h"
#ifdef __cplusplus
}
#endif

#include "ether_ros2/ethercat_slave.hpp"

namespace ether_ros2
{
// Application parameters
#define CLOCK_TO_USE CLOCK_MONOTONIC
// #define LOGGING_SAMPLING
// #define RUN_TIME 60 // run time in seconds
// #if defined(LOGGING) && defined(LOGGING_SAMPLING)
// #define SAMPLING_FREQ 10
// #endif
/****************************************************************************/

#define NSEC_PER_SEC (1000000000L)
// #define PERIOD_NS (NSEC_PER_SEC / FREQUENCY)
#define DIFF_NS(A, B) \
  (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + (B).tv_nsec - (A).tv_nsec)

#define TIMESPEC2NS(T) ((uint64_t)(T).tv_sec * NSEC_PER_SEC + (T).tv_nsec)

/** Return the sign of a number
 *
 * ie -1 for -ve value, 0 for 0, +1 for +ve value
 *
 * \retval the sign of the value
 */
#define sign(val)              \
  ({                           \
    typeof(val) _val = (val);  \
    ((_val > 0) - (_val < 0)); \
  })

#define handle_error_en(en, msg) \
  do {                           \
    errno = en;                  \
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),  msg);              \
    exit(EXIT_FAILURE);          \
  } while (0)

/****************************************************************************/

class EtherCATNetwork {  //<---------- Underscore & struct remain
 public:
  ec_master_t* master_;
  ec_master_state_t master_state_;
  ec_master_info_t master_info_;
  ec_domain_t* domain1_;
  ec_domain_state_t domain1_state_;
  std::shared_ptr<uint8_t> domain1_pd_;
  std::shared_ptr<uint8_t[]> process_data_buf_;
  size_t total_process_data_;
  int log_fd_;

  struct yaml_pars {
    std::string slv_name;
    std::map<std::string, unsigned int> fields;
  };

  std::vector<yaml_pars> yaml_net_cfg_;
  void SlaveParse();
  void init();
  void PrintNetInfo();
  std::uint32_t PERIOD_NS_;
  unsigned int FREQUENCY_;
  std::uint32_t RUN_TIME_;

  EtherCATNetwork(ec_master_t* m) : master_(m) {
    if (!master_) {
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Failed to get master.\n");
      exit(1);
    }

    std::cout << "Master: " << master_ << std::endl;

    domain1_ = ecrt_master_create_domain(master_);
    if (!domain1_) 
    {
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Failed to create domain.\n");
      exit(1);
    }

    if (ecrt_master(master_, &master_info_) != 0) {
      handle_error_en(ecrt_master(master_, &master_info_), "ecrt_master_info");
    }
  }
};

extern EtherCATNetwork ecat_network;
extern pthread_spinlock_t lock;
extern std::vector<EthercatSlave> ecat_slaves;

}

// #ifdef LOGGING
// extern statistics_struct stat_struct;
// #endif
#endif /* ether_ros_LIB_H */
