/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 Control Systems Lab, Legged Robots Team, NTUA
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *  Contact information: csl.legged.ntua@gmail.com
 *****************************************************************************/
/**
   \file ethercat_slave.h
   \brief Header file for the EthercatSlave class.
*/

/*****************************************************************************/

#ifndef ETH_SLAVE_LIB_H
#define ETH_SLAVE_LIB_H

#include <string>

#ifdef __cplusplus
extern "C"
{
#endif
# include "ecrt.h"
#ifdef __cplusplus
}
#endif
#include "rclcpp/rclcpp.hpp"

namespace ether_ros2
{
/** \class EthercatSlave
    \brief The Ethercat Slave class.

    Used for having all the ethercat slave related variables,
    fetched from the correspondent yaml file, in a single entity.
*/
class EthercatSlave {
 private:
  std::string slave_id_;
  std::uint8_t slave_group_;
  ec_slave_info_t slave_info_;
  ec_slave_config_t
      *ethercat_slave_;  // pointer to the basic slave struct in ighm
  int pdo_in_;
  int pdo_out_;
  int32_t sync0_shift_;
  int32_t sync1_shift_;

 public:
  /** \fn void init(std::string slave, ros::NodeHandle& n)
  \brief Initialization Method.

  Used for initializing the EthercatSlave entity. It's basically
  the main method in the class.
*/
  /** \fn int get_pdo_out()
  \brief Getter Method.

  Used for getting the number of bytes of the output PDO of the single slave.
*/
  /** \fn int get_pdo_in()
  \brief Getter Method.

  Used for getting the number of bytes of the input PDO of the single slave.
*/


  EthercatSlave(unsigned int k);

  int GetPdoOut();
  int GetPdoIn();
  size_t GetPdoOutSize();
  size_t GetPdoInSize();
  std::uint8_t GetSlaveGroup();
  ec_slave_config_t *get_slave_config();
  ec_slave_info_t get_slave_info();
  std::string GetSlaveID();
};

}

#endif /* ETH_SLAVE_LIB_H */
