# This script was created to automate kernel patching procedure.
# Nevertheless some settings are set manually and is strongly
# recommended to consult: https://stackoverflow.com/questions/51669724/install-rt-linux-patch-for-ubuntu
# To run the script: sudo ./install_RT_patched_Kernel.sh  [Patch Version]
# e.g.: "sudo ./install_RT_patched_Kernel.sh 5.4.69-rt39" to install Patched RT Kernel 5.4.69 with patch-5.4.69-rt39.patch.gz   

#   CSL-EP Legged Robots Team - National Technical University of Athens

#   Developers:
#               Thanasis Mastrogeorgiou, PhD Student
#               Aristotelis Papatheodorou, Diploma Student


# Install necessary packages


# Find Kernel & Patch's Major Version Index 
patch_vers=$1;
patch_versidx=`echo $patch_vers|  grep -e  "^[0-9].[0-9]*" -o`;
kern_versidx=`echo $patch_vers |  grep -e  "^[0-9.]" -o`;
kern_vers=`echo $patch_vers | grep -e  "^[0-9.]*" -o`
# echo $patch_vers
# echo $kern_versidx;
# echo $patch_versidx;
# echo $kern_vers;

# Create & Navigate to Kernel's Working Directory
if [ -d ~/kernel ]
then
    rm -vrf ~/kernel;
fi

mkdir ~/kernel && cd ~/kernel;

# Download the desired Kernel flavour
 wget https://www.kernel.org/pub/linux/kernel/v"$kern_versidx".x/linux-"$kern_vers".tar.gz &&
 wget https://www.kernel.org/pub/linux/kernel/projects/rt/"$patch_versidx"/patch-"$patch_vers".patch.gz ;

sudo apt-get build-dep linux
sudo apt-get install libncurses-dev flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf fakeroot


# Unzip the kernel.
tar -xzvf linux-"$kern_vers".tar.gz;

# Patch the kernel.
cd linux-"$kern_vers" &&
gzip -cd ../patch-"$patch_vers".patch.gz | patch -p1 --verbose;

cp /boot/config-$(uname -r) .config;
# Enable Realtime Processing.
yes '' | make oldconfig;
make menuconfig; #      <---------------------- The user needs to set manually the rt options. (Fully Preemptive)


# Compile the Kernel.
make -j `nproc` deb-pkg
# sudo make modules_install -j8;
# sudo make install -j8;

# #Make kernel images lighter.
# cd /lib/modules/"$patch_vers"; # or your new kernel
# sudo find . -name *.ko -exec strip --strip-unneeded {} +

# # Change the compression format

# #sudo vi /etc/initramfs-tools/initramfs.conf;
# #Modify COMPRESS=lz4 to COMPRESS=xz (line 53)
# # COMPRESS=xz ;
# # [:wq];

# sudo sed -i "s/COMPRESS=lz4/COMPRESS=xz/" /etc/initramfs-tools/initramfs.conf;
# sudo update-initramfs -u;

echo && echo "Check that the correct versions are present:"
ls ../*deb
echo " ";
echo "Press Any Key to Continue..."
read -n 1; #Wait for key press

sudo dpkg -i ../*.deb
# # Update grub

# sudo update-grub2;
# # Reboot the system.
sudo reboot;
