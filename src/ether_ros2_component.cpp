/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2021 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS 2
 *  environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *
 *****************************************************************************/



#include <chrono>
#include <iostream>
#include <memory>
#include <utility>

#include "rclcpp/rclcpp.hpp"
#include "ether_ros2/ether_ros2_component.hpp"

#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "lifecycle_msgs/msg/transition.hpp"
#include "ether_ros2/msg/pdo_in.hpp"
#include "ether_ros2/msg/pdo_in_xmc.hpp"
#include "ether_ros2/msg/pdo_raw.hpp"
#include "ether_ros2/msg/modify_pdo_variables.hpp"

#include "ether_ros2/ethercat_slave.hpp"
#include "ether_ros2/ethercat_network.hpp"


#include <fstream>
#include <map>
#include <string>
#include <vector>

#include "ether_ros2/utilities.hpp"
#include "yaml-cpp/yaml.h"

namespace ether_ros2
{

using namespace std::chrono_literals;

//
// EtherCAT Communicator Constructor
//
EthercatCommunicator::EthercatCommunicator(rclcpp::NodeOptions options) : 
  rclcpp_lifecycle::LifecycleNode("ether_ros2", options.use_intra_process_comms(true))
{
  RCLCPP_INFO(this->get_logger(), "This process is %d\n", getpid());
  RCLCPP_INFO(this->get_logger(), "The real user ID is %d\n", getuid());
  RCLCPP_INFO(this->get_logger(), "The effective user ID is %d\n", geteuid());
}

EthercatCommunicator::~EthercatCommunicator()
{
  ;
}

//
// Lifecycle Node's Configuration Callback
//
rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn 
          EthercatCommunicator::on_configure(const rclcpp_lifecycle::State & previous_state)
{
  //
  // Lock Program's Memory    
  //
  if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1) {
    RCLCPP_FATAL(this->get_logger(), "mlockall failed");
    exit(1);
  }

  // It doesn't matter for our application what pshared value we use
  if (pthread_spin_init(&lock, PTHREAD_PROCESS_SHARED) != 0) {
    handle_error_en(pthread_spin_init(&lock, PTHREAD_PROCESS_SHARED),
                    "pthread_spin_lock_init");
  }

  ecat_network.SlaveParse();

  for (unsigned int i = 0; i < ecat_network.master_info_.slave_count; i++) {
    ecat_slaves.push_back(i);
  }
  
  ecat_network.init();

  //
  // Initialize EtherCAT Communicator
  //
  ecat_comm_init();
  //
  // Initialize PDO In Publihser
  //
  pdo_in_init();

  //
  // Initialize PDO Out Listener
  //
  pdo_out_listener_init();

  //
  // Initialize  PDO Out Publisher
  //
  // pdo_out_publisher_init();



  RCLCPP_INFO(this->get_logger(), "on_configure() is called.");

  // We return a success and hence invoke the transition to the next
  // step: "inactive".
  // If we returned TRANSITION_CALLBACK_FAILURE instead, the state machine
  // would stay in the "unconfigured" state.
  // In case of TRANSITION_CALLBACK_ERROR or any thrown exception within
  // this callback, the state machine transitions to state "errorprocessing".
  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

//
// Lifecycle Node's Activation Callback
//
rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn 
      EthercatCommunicator::on_activate(const rclcpp_lifecycle::State & previous_state)
{
  //
  // Callback's return Result Initialization
  //
  auto ret = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;

  pdo_raw_pub_->on_activate();


  for (unsigned int i = 0; i < ecat_network.master_info_.slave_count; i++) {
    pdo_in_pub_.at(i)->on_activate();
  }

  //
  // Start EtherCAT Communicator
  //
  if(start())
  {
    //
    // Unable to start EtherCAT Communicator
    //
    RCLCPP_INFO(this->get_logger(), "Unable to start");
    ret = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
  }

  //
  // Return Activating result
  //
  return ret;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn 
          EthercatCommunicator::on_deactivate(const rclcpp_lifecycle::State & previous_state)
{
  //
  // Callback's return Result Initialization
  //
  auto ret = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;


  pdo_raw_pub_->on_deactivate();

  for (unsigned int i = 0; i < ecat_network.master_info_.slave_count; i++) {
    pdo_in_pub_.at(i)->on_deactivate();
  }
  //
  // Stop EtherCAT Communicator
  //
  if(stop())
  {
    //
    // Unable to stop EtherCAT Communicator
    //
    ret = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
  }

  //
  // Return Activating result
  //
  return ret;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn 
          EthercatCommunicator::on_shutdown(const rclcpp_lifecycle::State & previous_state)
{
  //
  // Callback's return Result Initialization
  //
  auto ret = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;

  //
  // Unlock Program's Memory
  //
  if (munlockall() == -1) {
    RCLCPP_FATAL(get_logger(), "munlockall failed");
    exit(1);
  }

  //
  // Clean node's elements
  //
  ClearNodeElements();

  return ret;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn 
          EthercatCommunicator::on_cleanup(const rclcpp_lifecycle::State & previous_state)
{

  RCLCPP_INFO(this->get_logger(), "Clean-up handler has been called!");

  return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

void EthercatCommunicator::ClearNodeElements(void)
{
  //
  // Clear Slaves' Vector
  //
  ecat_slaves.clear();

  //
  // Clear Network's YAML Configuration Vector
  //
  ecat_network.yaml_net_cfg_.clear();

  //
  // Clear Subscriptions
  //
  pdo_out_listener_.reset();
  pdo_in_raw_sub_.reset();
  pdo_out_raw_sub_.reset();


  //
  // Clear Publishers
  //
  pdo_raw_pub_.reset();
  pdo_in_pub_.clear();
  pdo_out_timer_pub_.reset();

}

}

//#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
//RCLCPP_COMPONENTS_REGISTER_NODE(ether_ros2::EthercatCommunicator)


int main(int argc, char * argv[])
{
  // force flush of the stdout buffer.
  // this ensures a correct sync of all prints
  // even when executed simultaneously within the launch file.
  setvbuf(stdout, NULL, _IONBF, BUFSIZ);

  rclcpp::init(argc, argv);

  rclcpp::executors::MultiThreadedExecutor exe;
  rclcpp::NodeOptions options;

  options.use_intra_process_comms(true);

  std::shared_ptr<ether_ros2::EthercatCommunicator> lc_node =
    std::make_shared<ether_ros2::EthercatCommunicator>(options);

  exe.add_node(lc_node->get_node_base_interface());
  std::cout << "spin" << std::endl;
  exe.spin();
  std::cout << "shutdown" << std::endl;
  rclcpp::shutdown();
  std::cout << "end" << std::endl;
  return 0;
}

// // // Create a Talker "component" that subclasses the generic rclcpp::Node base class.
// // // Components get built into shared libraries and as such do not write their own main functions.
// // // The process using the component's shared library will instantiate the class as a ROS node.
// // Talker::Talker(const rclcpp::NodeOptions & options)
// // : Node("talker", options), count_(0)
// // {
// //   // Create a publisher of "std_mgs/String" messages on the "chatter" topic.
// //   pub_ = create_publisher<std_msgs::msg::String>("chatter", 10);

// //   // Use a timer to schedule periodic message publishing.
// //   timer_ = create_wall_timer(1s, std::bind(&Talker::on_timer, this));
// // }

// // void Talker::on_timer()
// // {
// //   auto msg = std::make_unique<std_msgs::msg::String>();
// //   msg->data = "Hello World: " + std::to_string(++count_);
// //   RCLCPP_INFO(this->get_logger(), "Publishing: '%s'", msg->data.c_str());
// //   std::flush(std::cout);

// //   // Put the message into a queue to be processed by the middleware.
// //   // This call is non-blocking.
// //   pub_->publish(std::move(msg));
// // }

//}  // namespace composition

// // #include "rclcpp_components/register_node_macro.hpp"

// // // Register the component with class_loader.
// // // This acts as a sort of entry point, allowing the component to be discoverable when its library
// // // is being loaded into a running process.
// // RCLCPP_COMPONENTS_REGISTER_NODE(composition::Talker)
