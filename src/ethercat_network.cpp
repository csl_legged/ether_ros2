/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *
 *
 *****************************************************************************/

#include "rclcpp/rclcpp.hpp"
#include "ether_ros2/ether_ros2_component.hpp"
#include "ether_ros2/ethercat_network.hpp"
#include "ether_ros2/ethercat_slave.hpp"

#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "ether_ros2/utilities.hpp"
#include "yaml-cpp/yaml.h"

#include <ament_index_cpp/get_package_share_directory.hpp>

namespace ether_ros2
{

/*****************************************************************************/

/************************GLOBAL VARIABLES ************************************/
EtherCATNetwork ecat_network(ecrt_request_master(0));
pthread_spinlock_t lock;

std::vector<EthercatSlave> ecat_slaves;

void EtherCATNetwork::SlaveParse() {
  std::string filePath = "/home/mobile/dev_ws/src/ether_ros2/config/ethercat_slaves.yaml";
   //   ament_index_cpp::get_package_share_directory("ether_ros2") + "/config/ethercat_slaves.yaml";
  std::cout << "YAML Location: " << filePath << std::endl;
  YAML::Node basenode = YAML::LoadFile(filePath);
  yaml_pars slave;

  for (auto it = basenode.begin(); it != basenode["period_ns"].end(); ++it) {
    if (yaml_net_cfg_.size() < basenode.size() - 2) {
      std::cout << "\n" << it->first.as<std::string>() << "\n";
      slave.slv_name = it->first.as<std::string>();
      for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
        slave.fields.emplace(it2->first.as<std::string>(),
                             it2->second.as<unsigned int>());
      }
      yaml_net_cfg_.push_back(slave);
      slave.fields.clear();
    } else {
      PERIOD_NS_ = it->second.as<std::uint32_t>();
      ++it;
      RUN_TIME_ = it->second.as<std::uint32_t>();
      break;
    }
  }

  std::cout << "PERIOD_NS: " << PERIOD_NS_ << std::endl;
  std::cout << "RUN_TIME: " << RUN_TIME_ << std::endl;

  if (yaml_net_cfg_.empty()) {
    RCLCPP_FATAL(rclcpp::get_logger("rclcpp"), "YAML PARSING FAILED");
    exit(1);
  }

  if (master_info_.slave_count > yaml_net_cfg_.size()) {
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "MORE SLAVES IN THE BUS THAN THOSE CONFIGURED IN YAML!");
    exit(1);
  } else if (master_info_.slave_count < yaml_net_cfg_.size()) {
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "FEWER SLAVES IN THE BUS THAN THOSE CONFIGURED IN YAML");
  }

  std::cout << "Parsed slaves: " << yaml_net_cfg_.size() << "\n";
  for (auto it_slv = yaml_net_cfg_.begin(); it_slv != yaml_net_cfg_.end();
       ++it_slv) {
    std::cout << "Slave Name: " << it_slv->slv_name << std::endl;
    for (auto it_fld = it_slv->fields.begin(); it_fld != it_slv->fields.end();
         ++it_fld) {
      std::cout << it_fld->first << ": " << it_fld->second << std::endl;
    }
    std::cout << std::endl;
  }
}

void EtherCATNetwork::init(void) {
  FREQUENCY_ = (NSEC_PER_SEC / PERIOD_NS_);

  total_process_data_ = ecrt_domain_size(domain1_);

  /******************************************
   *    Application domain data              *
   *******************************************/
  PrintNetInfo();
  process_data_buf_ = std::make_unique<uint8_t[]>(total_process_data_);
}

void EtherCATNetwork::PrintNetInfo() {
  std::cout << "EtherCAT Network's General Information:" << std::endl;
  std::cout << "Total Number of Slaves in the bus: " << master_info_.slave_count
            << std::endl;
  std::cout << "Total Number of Process Data Bytes: " << total_process_data_
            << std::endl;

  for (auto it = ecat_slaves.begin(); it != ecat_slaves.end(); ++it) {
    std::cout << "EtherCAT Slave's " << it->GetSlaveID() << std::endl;
    std::cout << "PDO In Size (#Bytes) " << it->GetPdoInSize() << std::endl;
    std::cout << "PDO Out Size (#Bytes) " << it->GetPdoOutSize() << std::endl;
  }
}

}

/*****************************************************************************/

/** @} */
