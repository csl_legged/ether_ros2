﻿/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *****************************************************************************/
/**
   \file ethercat_slave.cpp
   \brief Implementation of EthercatSlave class.

   Used for containing all the useful information
   of an EtherCAT slave, from the userspace program perspective. Receives all
   the useful information via the ROS Parameter Server (after they are loaded
   from ethercat_slaves.yaml).
*/

/*****************************************************************************/
#include "ether_ros2/ethercat_slave.hpp"
#include "ether_ros2/ethercat_network.hpp"

#include "rclcpp/rclcpp.hpp"

#include <iostream>
#include "yaml-cpp/yaml.h"

namespace ether_ros2
{
  
EthercatSlave::EthercatSlave(unsigned int k) 
{
  const EtherCATNetwork::yaml_pars slv_cfg = ecat_network.yaml_net_cfg_.at(k);
  ethercat_slave_ = ecrt_master_slave_config(
      ecat_network.master_, slv_cfg.fields.find("alias")->second,
      slv_cfg.fields.find("position")->second,
      slv_cfg.fields.find("vendor_id")->second,
      slv_cfg.fields.find("product_code")->second);
  if (!ethercat_slave_) 
  {
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Failed to get slave configuration.\n");
    exit(1);
  }
  pdo_out_ = ecrt_slave_config_reg_pdo_entry(
      ethercat_slave_, slv_cfg.fields.find("output_port")->second, 1,
      ecat_network.domain1_, NULL);
  if (pdo_out_ < 0) 
  {
    RCLCPP_FATAL(rclcpp::get_logger("rclcpp"), "Failed to configure pdo out.\n");
    exit(1);
  }

  RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Offset pdo out is: %d\n", pdo_out_);

  pdo_in_ = ecrt_slave_config_reg_pdo_entry(
      ethercat_slave_, slv_cfg.fields.find("input_port")->second, 1,
      ecat_network.domain1_, NULL);

  if (pdo_in_ < 0) 
  {
    RCLCPP_FATAL(rclcpp::get_logger("rclcpp"), "Failed to configure pdo in.\n");
    exit(1);
  }
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Offset pdo in is: %d\n", pdo_in_);

  sync0_shift_ = slv_cfg.fields.find("sync0_shift")->second;
  sync1_shift_ = slv_cfg.fields.find("sync1_shift")->second;

  if (slv_cfg.fields.find("sync1_shift")->second !=
      slv_cfg.fields.end()->second) 
  {

    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "SYNC1 CYCLE DEFINED");
    ecrt_slave_config_dc(ethercat_slave_,
                         slv_cfg.fields.find("assign_activate")->second,
                         ecat_network.PERIOD_NS_, sync0_shift_,
                         ecat_network.PERIOD_NS_, sync1_shift_);
  } 
  else 
  {
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "SYNC1 CYCLE IS UNDEFINED.Only SYNC0 will be used!");
    ecrt_slave_config_dc(ethercat_slave_,
                         slv_cfg.fields.find("assign_activate")->second,
                         ecat_network.PERIOD_NS_, sync0_shift_, 0, 0);
  }

  slave_id_ = slv_cfg.slv_name;
  slave_group_ = slv_cfg.fields.find("slave_group")->second;
  ecrt_master_get_slave(ecat_network.master_,
                        slv_cfg.fields.find("position")->second, &slave_info_);

  // configure SYNC signals for this slave
  // For XMC use: 0x0300
  // For Beckhoff FB1111 use: 0x0700
  // Use PERIOD_NS as the period, and 50 μs shift time
  // ecrt_slave_config_dc(ethercat_slave_, assign_activate_, PERIOD_NS,
  // sync0_shift_, 0, 0);
}

int EthercatSlave::GetPdoIn() 
{ 
  return pdo_in_; 
}

int EthercatSlave::GetPdoOut() 
{ 
  return pdo_out_; 
}

size_t EthercatSlave::GetPdoOutSize() {
  return std::uint16_t(pdo_in_ - pdo_out_);
}

size_t EthercatSlave::GetPdoInSize() 
{
  std::uint16_t res_tmp = 0;
  if (slave_info_.position < ecat_network.master_info_.slave_count - 1) {
    res_tmp = ecat_slaves.at(slave_info_.position + 1).GetPdoOut() - pdo_in_;
  } 
  
  else 
  {
    res_tmp = ecat_network.total_process_data_ - pdo_in_;
  }
  
  return res_tmp;
}

ec_slave_config_t *EthercatSlave::get_slave_config() 
{ 
  return ethercat_slave_; 
}
ec_slave_info_t EthercatSlave::get_slave_info() 
{
  return slave_info_; 
}
std::string EthercatSlave::GetSlaveID() 
{ 
  return slave_id_; 
}
std::uint8_t EthercatSlave::GetSlaveGroup() 
{ 
  return slave_group_; 
}

}