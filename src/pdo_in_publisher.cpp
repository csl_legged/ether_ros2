/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *****************************************************************************/
/**
   \file pdo_in_publisher.cpp
   \brief Implementation of PDOInPublisher class.

   Used for publishing the "raw" input data, received from EtherCAT Communicator
   after transformation into useful, human-readable format, consisted of the
   EtherCAT variables used by our application. Transforms the indeces to
   variables.
*/

/*****************************************************************************/
#include "rclcpp/rclcpp.hpp"
#include "ether_ros2/ether_ros2_component.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "ether_ros2/msg/pdo_in.hpp"
#include "ether_ros2/msg/pdo_in_xmc.hpp"
#include "ether_ros2/msg/pdo_raw.hpp"

#include "ether_ros2/ethercat_slave.hpp"
#include "ether_ros2/ethercat_network.hpp"
#include "ether_ros2/utilities.hpp"

namespace ether_ros2
{
void EthercatCommunicator::pdo_in_raw_callback(ether_ros2::msg::PdoRaw::UniquePtr pdo_raw) 
{
   std::vector<std::uint8_t> pdo_in_raw = pdo_raw->pdo_in_raw;
   std::uint8_t *data_ptr;
   std::size_t pdo_pos = 0;
   using namespace utilities;

   for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) 
   {
      // std::weak_ptr<std::remove_pointer<decltype(pdo_in_pub_.at(i).get())>::type> captured_pub = pdo_in_pub_.at(i);

      //
      // Process ONLY the sections of vector related to Laelaps II Leg's Slaves
      //
      data_ptr = (std::uint8_t *)&pdo_in_raw[pdo_pos];

      switch (ecat_slaves.at(i).GetSlaveGroup()) 
      {
         case 0: 
         {
            // std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PDOIn>> pub_ptr = captured_pub.lock();

            if(pdo_in_pub_.at(i))
            {
            ether_ros2::msg::PdoIn::UniquePtr pdo_in(new ether_ros2::msg::PdoIn());

            //
            // Laelaps II Leg Slave PDO Inputs
            //
            pdo_in->hip_angle = process_input_int16(data_ptr, 0);
            pdo_in->desired_hip_angle = process_input_int16(data_ptr, 2);
            pdo_in->time = process_input_uint16(data_ptr, 4);
            pdo_in->knee_angle = process_input_int16(data_ptr, 6);
            pdo_in->desired_knee_angle = process_input_int16(data_ptr, 8);
            pdo_in->pwm_knee = process_input_int16(data_ptr, 10);
            pdo_in->pwm_hip = process_input_int16(data_ptr, 12);
            pdo_in->velocity_knee = process_input_int32(data_ptr, 14);
            pdo_in->velocity_hip = process_input_int32(data_ptr, 18);
            pdo_in_pub_.at(i)->publish(std::move(pdo_in));
            }
            break;
         }
         case 1: 
         {

            //std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PDOInXMC>> pub_ptr = captured_pub.lock();

            if(pdo_in_pub_.at(i))
            {        
            // ether_ros2::msg::PDOInXMC::UniquePtr pdo_in_XMC(new ether_ros2::msg::PDOInXMC());

            //
            // Infineon's XMC4800 Slave
            //
            // pdo_in_XMC->in_gen_int1 = process_input_uint16(data_ptr, 0);
            // pdo_in_XMC->in_gen_int2 = process_input_uint16(data_ptr, 2);
            // pdo_in_XMC->in_gen_int3 = process_input_uint16(data_ptr, 4);
            // pdo_in_XMC->in_gen_int4 = process_input_uint16(data_ptr, 6);
            // pdo_in_XMC->in_gen_bit1 = process_input_bit(data_ptr, 8, 0);
            // pdo_in_XMC->in_gen_bit2 = process_input_bit(data_ptr, 0, 1);
            // pdo_in_XMC->in_gen_bit3 = process_input_bit(data_ptr, 0, 2);
            // pdo_in_XMC->in_gen_bit4 = process_input_bit(data_ptr, 0, 3);
            // pdo_in_XMC->in_gen_bit5 = process_input_bit(data_ptr, 0, 4);
            // pdo_in_XMC->in_gen_bit6 = process_input_bit(data_ptr, 0, 5);
            // pdo_in_XMC->in_gen_bit7 = process_input_bit(data_ptr, 0, 6);
            // pdo_in_XMC->in_gen_bit8 = process_input_bit(data_ptr, 0, 7);
            // pdo_in_pub_.at(i)->publish(std::move(pdo_in_XMC));
            }
            break;
         }
      }

      pdo_pos += ecat_slaves.at(i).GetPdoInSize();
   }
}

void EthercatCommunicator::pdo_in_init(void)
{
  pdo_in_raw_sub_ = this->create_subscription<ether_ros2::msg::PdoRaw>(
    "pdo_raw", 1000, std::bind(&EthercatCommunicator::pdo_in_raw_callback, this, std::placeholders::_1));

  for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) 
  {
    //
    // Determine Slave's PDO Input Category
    //
    switch (ecat_slaves.at(i).GetSlaveGroup()) {
      case 0: 
      {
        //
        // Laelaps II Leg Slave
        //
        pdo_in_pub_.push_back(this->create_publisher<ether_ros2::msg::PdoIn>(
                                "pdo_in_LegSlave_" + std::to_string(i), 1000));
        break;
      }
      case 1: 
      {
      //   //
      //   // Infineon's XMC4800 Slave
      //   //
      //   pdo_in_pub_.push_back(this->create_publisher<ether_ros2::msg::PdoInXmc>(
      //       "pdo_in_XmcSlave_" + std::to_string(i), 1000));
        break;
      }
    }
  }
}

}



// }
  
// // void PDOInPublisher::init() {
// //   // Create  ROS subscriber for the Ethercat RAW data
// //   pdo_raw_sub_ = n.subscribe("pdo_raw", 1000, &PDOInPublisher::pdo_raw_callback,
// //                              &pdo_in_publisher);

// //       pdo_raw_sub_ = this->create_subscription<ether_ros2:PDORaw>(input, 1000, &PDOInPublisher::pdo_raw_callback)

// //   // Create  ROS publishers for the Ethercat formatted data
// //   // pdo_in_pub_ = new ros::Publisher[ecat_network.master_info_.slave_count];
// //   for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) {
// //     //
// //     // Determine Slave's PDO Input Category
// //     //
// //     switch (ecat_slaves.at(i).GetSlaveGroup()) {
// //       case 0: {
// //         //
// //         // Laelaps II Leg Slave
// //         //
// //         pdo_in_pub_.push_back(n.advertise<ether_ros::PDOIn>(
// //             "pdo_in_LegSlave_" + std::to_string(i), 1000));
// //         break;
// //       }
// //       case 1: {
// //         //
// //         // Infineon's XMC4800 Slave
// //         //
// //         pdo_in_pub_.push_back(n.advertise<ether_ros::PDOInXMC>(
// //             "pdo_in_XmcSlave_" + std::to_string(i), 1000));
// //         break;
// //       }
// //     }
// //   }
// // }
