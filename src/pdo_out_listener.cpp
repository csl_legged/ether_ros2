/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *****************************************************************************/
/**
   \file pdo_out_publisher.cpp
   \brief Implementation of PDOOutPublisher class.

   Used for handling the "raw" output data, received from EtherCAT Communicator
   and transforming them into useful, human-readable format, consisted of the
   EtherCAT variables used by our application. Transforms the indeces to
   variables.
*/

/*****************************************************************************/

#include "rclcpp/rclcpp.hpp"
#include "ether_ros2/ether_ros2_component.hpp"

#include "ether_ros2/msg/modify_pdo_variables.hpp"
#include "ether_ros2/msg/pdo_raw.hpp"

#include "ether_ros2/ethercat_network.hpp"
#include "ether_ros2/ethercat_slave.hpp"

#include <pthread.h>

#include <iostream>
#include <string>
#include <vector>

#include "ether_ros2/utilities.hpp"

namespace ether_ros2
{
void EthercatCommunicator::pdo_out_callback(ether_ros2::msg::ModifyPdoVariables::SharedPtr new_var) {
  pthread_spin_lock(&lock);
  // check if we are broadcasting a variable's value to all slaves
  if (new_var->slave_pos < 0) {

      //
      // No valid position specified, check alias #
      //
    if (new_var->slave_group < 0) {

        //
        // Broadcast Variables to all of the Slave's in the Bus
        //
        std::cout << "Broadcasting Value to all Slaves in the Bus" << std::endl;
        for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) {
            modify_pdo_variable(i, new_var);
      }
    } else {

        //
        // Broadcast Variable to the specified alias Group ONLY
        //
        std::cout << "Broadcasting Value to the Designated Slave Group" << std::endl;
        for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) {
            if(ecat_slaves.at(i).GetSlaveGroup() == new_var->slave_group) {
                modify_pdo_variable(i, new_var);
            }
        }
    }
  } else {

      //
      // Broadcast Variable to the specified Slave ONLY
      //
      // std::cout << "Broadcasting Value to Slave: " << ecat_slaves.at(new_var->slave_pos).GetSlaveID() << std::endl;
      modify_pdo_variable(new_var->slave_pos, new_var);
  }
  pthread_spin_unlock(&lock);
}

void EthercatCommunicator::modify_pdo_variable(int pos, ether_ros2::msg::ModifyPdoVariables::SharedPtr new_var)
{
  std::string type = new_var->type;

  type = utilities::trim(type);

  switch (int_type_map_[type]) {
    case 0:

    {
      uint8_t *new_data_ptr =
          (ecat_network.process_data_buf_.get() +
           ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      bool value = new_var->bool_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_BIT(new_data_ptr, new_var->subindex, value);
      break;
    }

    case 1:

    {
      uint8_t *new_data_ptr =
          (uint8_t *)(ecat_network.process_data_buf_.get() +
                      ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      uint8_t value = new_var->uint8_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_U8(new_data_ptr, value);
      break;
    }

    case 2:

    {
      int8_t *new_data_ptr =
          (int8_t *)(ecat_network.process_data_buf_.get() +
                     ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      int8_t value = new_var->int8_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_S8(new_data_ptr, value);
      break;
    }

    case 3:

    {
      uint16_t *new_data_ptr =
          (uint16_t *)(ecat_network.process_data_buf_.get() +
                       ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      // printf("O:");
      // for(int j = 0 ; j < 24; j++)
      //     printf(" %2.2x", *(new_var->value[j]);
      // printf("\n");
      uint16_t value = new_var->uint16_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_U16(new_data_ptr, value);
      break;
    }

    case 4: {
      int16_t *new_data_ptr =
          (int16_t *)(ecat_network.process_data_buf_.get() +
                      ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      int16_t value = new_var->int16_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_S16(new_data_ptr, value);
      break;
    }

    case 5:

    {
      uint32_t *new_data_ptr =
          (uint32_t *)(ecat_network.process_data_buf_.get() +
                       ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      uint32_t value = new_var->uint32_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_U32(new_data_ptr, value);
      break;
    }
    case 6: {
      int32_t *new_data_ptr =
          (int32_t *)(ecat_network.process_data_buf_.get() +
                      ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      int32_t value = new_var->int32_value;
      // ROS_INFO("New value will be: %d\n", value);
      EC_WRITE_S32(new_data_ptr, value);
      break;
    }
    case 7:

    {
      uint64_t *new_data_ptr =
          (uint64_t *)(ecat_network.process_data_buf_.get() +
                       ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      uint64_t value = new_var->uint64_value;
      EC_WRITE_U64(new_data_ptr, value);
      break;
    }

    case 8:

    {
      int64_t *new_data_ptr =
          (int64_t *)(ecat_network.process_data_buf_.get() +
                      ecat_slaves.at(pos).GetPdoOut() + new_var->index);
      int64_t value = new_var->int64_value;
      EC_WRITE_S64(new_data_ptr, value);
      break;
    }
    default:
      RCLCPP_ERROR(this->get_logger(),
          "default handle called (shouldn't): check what you send to the "
          "/pdo_listener\n");
      break;
  }
}

void EthercatCommunicator::pdo_out_listener_init(void)
{
  pdo_out_listener_ = this->create_subscription<ether_ros2::msg::ModifyPdoVariables>(
                                        "pdo_listener", 1000,
                                          std::bind(&EthercatCommunicator::pdo_out_callback, this, std::placeholders::_1));
}

}
// void PDOOutListener::init(ros::NodeHandle &n) {
//   // Create  ROS subscriber for the Ethercat RAW data
//   pdo_out_listener_ =
//       n.subscribe("pdo_listener", 1000, &PDOOutListener::pdo_out_callback,
//                   &pdo_out_listener);
// }
