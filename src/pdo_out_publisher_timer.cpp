/******************************************************************************
 *
 *  $Id$
 *
 *  Copyright (C) 2020 NTUA CSL-EP LEGGED ROBOTS TEAM
 *
 *  This file is part of the IgH EtherCAT master userspace program in the ROS
 *environment.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is free
 *software; you can redistribute it and/or modify it under the terms of the GNU
 *General Public License as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  The IgH EtherCAT master userspace program in the ROS environment is
 *distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 *without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 *PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with the IgH EtherCAT master userspace program in the ROS environment.
 *If not, see <http://www.gnu.org/licenses/>.
 *
 *  ---
 *
 *  The license mentioned above concerns the source code only. Using the
 *  EtherCAT technology and brand is only permitted in compliance with the
 *  industrial property and similar rights of Beckhoff Automation GmbH.
 *****************************************************************************/
/**
   \file pdo_out_publisher.cpp
   \brief Implementation of PDOOutPublisher class.

    Used for streaming the "raw" \a pdo_out data inside the \a
   process_data_buffer to the \a /pdo_out_timer topic and transforming them into
   useful, human-readable format, consisted of the EtherCAT output variables
   used by our application, at a certain rate. It's been created for logging and
   debugging reasons.
*/

/*****************************************************************************/

#include "rclcpp/rclcpp.hpp"
#include "ether_ros2/ether_ros2_component.hpp"

#include <iostream>
#include <string>
#include <vector>

#include "ether_ros2/msg/pdo_out.hpp"
#include "ether_ros2/ethercat_slave.hpp"
#include "ether_ros2/utilities.hpp"

namespace ether_ros2
{

// void PDOOutPublisherTimer::timer_callback(std::weak_ptr<rclcpp_lifecycle::LifecyclePublisher<ether_ros2::msg::PDOOut>> captured_pub) {
//   std::size_t pos = 0;
//   data_ptr = std::make_shared<uint8_t[]>((size_t)ecat_network.total_process_data_);
//   std::uint8_t *data_pos = nullptr;
//   using namespace utilities;

//   auto pub_ptr = captured_pub.lock();

//   if (pub_ptr) 
//   {
//     copy_process_data_buffer_to_buf(data_ptr_.get());

//     for (std::uint8_t i = 0; i < ecat_network.master_info_.slave_count; i++) {
//       data_pos = (std::uint8_t *)(data_ptr_.get() + pos);
//       ether_ros2::msg::PDOOut::UniquePtr pdo_out;
//       pdo_out->slave_pos = i;
//       pdo_out->slave_group = ecat_slaves.at(i).GetSlaveGroup();
//       switch (std::uint8_t(pdo_out->slave_group)) {
//         case 0: {
//           //
//           // Laelaps II Leg Slave PDO Inputs
//           //
//           pdo_out->state_machine = process_input_bit(data_pos, 0, 0);
//           pdo_out->initialize_clock = process_input_bit(data_pos, 0, 1);
//           pdo_out->initialize_angles = process_input_bit(data_pos, 0, 2);
//           pdo_out->inverse_kinematics = process_input_bit(data_pos, 0, 3);
//           pdo_out->blue_led = process_input_bit(data_pos, 0, 4);
//           pdo_out->red_led = process_input_bit(data_pos, 0, 5);
//           pdo_out->laelaps_halt = process_input_bit(data_pos, 0, 6);
//           pdo_out->reverse_dir = process_input_bit(data_pos, 0, 7);
//           pdo_out->sync = process_input_int8(data_pos, 1);
//           pdo_out->desired_x_value = process_input_int32(data_pos, 2);
//           pdo_out->filter_bandwidth = process_input_uint16(data_pos, 6);
//           pdo_out->desired_y_value = process_input_int32(data_pos, 8);
//           pdo_out->kp_100_knee = process_input_int16(data_pos, 12);
//           pdo_out->kd_1000_knee = process_input_int16(data_pos, 14);
//           pdo_out->ki_100_knee = process_input_int16(data_pos, 16);
//           pdo_out->kp_100_hip = process_input_int16(data_pos, 18);
//           pdo_out->kd_1000_hip = process_input_int16(data_pos, 20);
//           pdo_out->ki_100_hip = process_input_int16(data_pos, 22);
//           pdo_out->x_cntr_traj1000 = process_input_int16(data_pos, 24);
//           pdo_out->y_cntr_traj1000 = process_input_int16(data_pos, 26);
//           pdo_out->a_ellipse100 = process_input_int16(data_pos, 28);
//           pdo_out->b_ellipse100 = process_input_int16(data_pos, 30);
//           pdo_out->traj_tf100 = process_input_int16(data_pos, 32);
//           pdo_out->traj_t100 = process_input_int16(data_pos, 34);
//           pdo_out->traj_ts100 = process_input_int16(data_pos, 36);
//           break;
//         }
//         case 1: {
//           //
//           // Infineon's XMC4800 Slave
//           //
//           pdo_out->out_gen_int1 = process_input_uint16(data_pos, 0);
//           pdo_out->out_gen_int2 = process_input_uint16(data_pos, 2);
//           pdo_out->out_gen_int3 = process_input_uint16(data_pos, 4);
//           pdo_out->out_gen_int4 = process_input_uint16(data_pos, 6);
//           pdo_out->out_gen_bit1 = process_input_bit(data_pos, 8, 0);
//           pdo_out->out_gen_bit2 = process_input_bit(data_pos, 8, 1);
//           pdo_out->out_gen_bit3 = process_input_bit(data_pos, 8, 2);
//           pdo_out->out_gen_bit4 = process_input_bit(data_pos, 8, 3);
//           pdo_out->out_gen_bit5 = process_input_bit(data_pos, 8, 4);
//           pdo_out->out_gen_bit6 = process_input_bit(data_pos, 8, 5);
//           pdo_out->out_gen_bit7 = process_input_bit(data_pos, 8, 6);
//           pdo_out->out_gen_bit8 = process_input_bit(data_pos, 8, 7);
//           break;
//         }
//       }
//       pub_ptr->publish(std::move(pdo_out));
//       pos += ecat_slaves.at(i).GetPdoOutSize();
//     }
//   }
// }

// void EthercatCommunicator::pdo_out_publisher_timer_init(void) {
//   // data_ptr_ = std::make_shared<uint8_t[]>((size_t)ecat_network.total_process_data_ * sizeof(std::uint8_t))
//   //     (std::uint8_t *)malloc(ecat_network.total_process_data_ * sizeof(std::uint8_t));

//   // std::fill(data_ptr_.get(),data_ptr_.get() + ecat_network.total_process_data_, 0);
//   // memset(data_ptr_, 0,
//   //        ecat_network.total_process_data_);  // fill the buffer with zeros
// //   std::unique_ptr<std::uint8_t[]> data_ptr2_ = std::make_unique<uint8_t[]>(ecat_network.total_process_data_);
//   // Create  ROS publisher for the Ethercat formatted data
//   pdo_out_pub_ = this->create_publisher<ether_ros2::msg::PDOOut>("pdo_out", 1000);


//   if (!pdo_out_pub_) {
//     RCLCPP_FATAL(this->get_logger(), "Unable to start publisher in ProcessDataTimer\n");
//     exit(1);
//   } else {
//     RCLCPP_FATAL(this->get_logger(), "Started ProcessDataTimer publisher\n");
//   }
//   pdo_out_timer_ = this->create_wall_timer(1s, std::bind(&EthercatCommunicator::timer_callback, this, pdo_out_pub_));
//   // Create  ROS timer
//   // first parameter is in seconds...

//   if (!pdo_out_timer_) 
//   {
//     RCLCPP_FATAL(this->get_logger(), "Unable to start ProcessDataTimer\n");
//     exit(1);
//   } 
  
//   else 
//   {
//     RCLCPP_INFO("Started ProcessDataTimer\n");
//   }
// }
}
